<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(DishSeeder::class);
        $this->call(UserFoodLogSeeder::class);
        $this->call(UserCarSeeder::class);
    }
}
