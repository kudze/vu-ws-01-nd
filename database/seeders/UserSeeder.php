<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            User::query()->create(
                [
                    'username' => 'random',
                    'password' => Hash::make('random'),
                    'role' => 'default'
                ]
            );

            User::query()->create(
                [
                    'username' => 'kudze',
                    'password' => Hash::make('temp'),
                    'role' => 'admin'
                ]
            );
        } catch(QueryException) {
            //silent fail.
        }
    }
}
