# Karolis Kraujelis Web Servizai 01 Laboratorinis

## Aprašymas

Servizas yra labai paprastas, yra varototojai kurie gali supildyti patiekalų sarašą ir jų vartojimo istorijos sąrašą.
Tada pagal įvestus duomenis galime paskaičiuoti įvairią statistiką.

Pagal antrą labaratorinį panaudojau CarRegistryRestAPI servizą: https://github.com/RafalKLab/CarRegistryRestApi
Vartotojai prie sistemos gali pridėti savo automobilius, ir prie statistikos paskaičiuoti jų skaičių.

## Įrašymas

Norint pasileisti reikia:

1. `git clone https://gitlab.com/kudze/vu-ws-01-nd.git` - Nuklonuoja git repositoriją.
2. `cd vu-ws-01-nd` - Ieiname i atsiusta direktorija.
3. `git submodule init` - Sukurs tuscia git repositorija, kur bus kito studento panaudotas darbas.
4. `git submodule update` - Atpullins commita git repozitorijos, kur yra kito studento panaudotas darbas.
5. `docker-compose up -d` - Paleidžia visus reikiamus servizus. Bus sukurtas admin user'is su prisijungimais (kudze : test)

## Dependencies

* Docker
* Docker-Compose
* Git

## Postman

Per čia galima prisijungti prie postman resursų: nebėra
