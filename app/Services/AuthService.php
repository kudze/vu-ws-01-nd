<?php

namespace App\Services;

use App\Exceptions\InvalidPasswordException;
use App\Exceptions\UserDeletionFailedException;
use App\Exceptions\UserTokenCreationFailedException;
use App\Models\Dish;
use App\Models\User;
use App\Models\UserCar;
use App\Models\UserFoodLog;
use App\Models\UserToken;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    private UserTokenFactory $tokenFactory;

    public function __construct(UserTokenFactory $tokenFactory)
    {
        $this->tokenFactory = $tokenFactory;
    }

    public function register(string $username, string $password): User
    {
        $password = Hash::make($password);

        /** @var User $user */
        $user = User::query()->create([
            'username' => $username,
            'password' => $password
        ]);

        return $user;
    }

    /**
     * Tries to login, on failure throws exception.
     *
     * @param string $username
     * @param string $password
     * @return UserToken
     *
     * @throws InvalidPasswordException|ModelNotFoundException|UserTokenCreationFailedException
     */
    public function login(string $username, string $password): UserToken
    {
        /** @var User $user */
        $user = User::query()->where('username', $username)->firstOrFail();

        if (!Hash::check($password, $user->getAttribute('password')))
            throw new InvalidPasswordException();

        return $this->tokenFactory->createNewAccessTokenForUser($user);
    }

    /**
     * This will create new Token for given user and remove passed token.
     *
     * @param User $user
     * @param string $token
     * @return UserToken
     */
    public function refreshTokenForUser(User $user, string $token): UserToken
    {
        $newToken = $this->tokenFactory->createNewAccessTokenForUser($user);

        UserToken::query()->where('token', $token)->delete();

        return $newToken;
    }

    public function getUserByToken(string $token): ?User
    {
        return UserToken::with('user')->where('token', $token)->first()?->user;
    }

    /**
     * Deletes an user.
     *
     * @param User $user
     *
     * @throws UserDeletionFailedException
     */
    public function deleteUser(User $user)
    {
        $res = DB::transaction(function () use ($user) {
            UserToken::query()->where('user_id', Auth::id())->delete();
            UserFoodLog::query()->where('user_id', Auth::id())->delete();
            UserCar::query()->where('user_id', Auth::id())->delete();
            Dish::query()->where('user_id', Auth::id())->update(['user_id' => null]);
            $user->delete();

            return 'OK';
        }, 5);

        if ($res != 'OK')
            throw new UserDeletionFailedException();
    }

}
