<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserFoodLog;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class StatisticsRepository
{

    public function calculateUserCalories(User $user, string $from, string $until): float
    {
        return UserFoodLog::query()
            ->join('dish', 'user_food_log.dish_id', '=', 'dish.id')
            ->where('user_food_log.user_id', $user->getKey())
            ->whereBetween('eaten_at', [$from, $until])
            ->sum('dish.calories');
    }

    public function calculateUserDishes(User $user, string $from, string $until): Collection
    {
        return UserFoodLog::query()
            ->join('dish', 'user_food_log.dish_id', '=', 'dish.id')
            ->select(DB::raw('dish.title, count(*) as count'))
            ->where('user_food_log.user_id', $user->getKey())
            ->whereBetween('eaten_at', [$from, $until])
            ->groupBy('dish_id')
            ->get();
    }

    public function calculateUserCars(User $user): int
    {
        $user->loadCount('cars');
        return $user->getAttribute('cars_count');
    }

}
