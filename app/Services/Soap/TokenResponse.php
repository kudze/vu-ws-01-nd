<?php

namespace App\Services\Soap;

use App\Models\UserToken;

class TokenResponse
{
    public string $token;
    public string $created_from_ip;
    public string $valid_till;

    public function __construct(UserToken $token)
    {
        $this->token = $token->getAttribute('token');
        $this->created_from_ip = $token->getAttribute('created_from_ip');
        $this->valid_till = $token->getAttribute('valid_till');
    }
}
