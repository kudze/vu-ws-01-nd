<?php

namespace App\Services\Soap;

use App\Models\UserFoodLog;

class FoodLogResponse
{
    public int $id;
    public string $created_at;
    public string $updated_at;
    public int $dish_id;
    public int $user_id;
    public string $eaten_at;

    public function __construct(UserFoodLog $foodLog)
    {
        $this->id = $foodLog->getAttribute('id');
        $this->created_at = $foodLog->getAttribute('created_at');
        $this->updated_at = $foodLog->getAttribute('updated_at') ?? '';
        $this->dish_id = $foodLog->getAttribute('dish_id');
        $this->user_id = $foodLog->getAttribute('user_id');
        $this->eaten_at = $foodLog->getAttribute('eaten_at');
    }
}
