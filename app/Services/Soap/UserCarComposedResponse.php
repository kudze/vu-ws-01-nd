<?php

namespace App\Services\Soap;

use App\Models\UserCar;

class UserCarComposedResponse extends UserCarResponse
{
    public UserResponse $user;
    public CarRegistryResponse $registry;

    public function __construct(UserCar $userCar, array $carRegistry)
    {
        parent::__construct($userCar);

        $this->user = new UserResponse($userCar->user);
        $this->registry = new CarRegistryResponse($carRegistry);
    }

}
