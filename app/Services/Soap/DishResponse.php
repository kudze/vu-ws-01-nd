<?php

namespace App\Services\Soap;

use App\Models\Dish;

class DishResponse
{
    public int $id;
    public string $title;
    public int $calories;
    public ?int $user_id;
    public string $created_at;
    public string $updated_at;

    public function __construct(Dish $dish)
    {
        $this->id = $dish->getAttribute('id');
        $this->title = $dish->getAttribute('title');
        $this->calories = $dish->getAttribute('calories');
        $this->user_id = $dish->getAttribute('user_id');
        $this->created_at = $dish->getAttribute('created_at');
        $this->updated_at = $dish->getAttribute('updated_at') ?? '';
    }
}
