<?php

namespace App\Services\Soap;

use App\Exceptions\InvalidPasswordException;
use App\Exceptions\UserDeletionFailedException;
use App\Exceptions\UserTokenCreationFailedException;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DishController;
use App\Http\Controllers\FoodLogController;
use App\Http\Controllers\StatisticsController;
use App\Http\Controllers\UserCarController;
use App\Models\Dish;
use App\Models\User;
use App\Models\UserCar;
use App\Models\UserFoodLog;
use App\Services\AuthService;
use App\Services\CarRegistryAPIService;
use App\Services\StatisticsRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\Validator as ValidatorInstance;
use SoapFault;

/**
 * @WSDL\Annotation\WebService(name="WebServiceAnnotations", targetNamespace="http://localhost/soap?wsdl", location="http://localhost/soap", ns="http://localhost/soap?wsdl")
 * @WSDL\Annotation\BindingType(value="SOAP_12")
 * @WSDL\Annotation\SoapBinding(style="DOCUMENT", use="ENCODED", parameterStyle="WRAPPED")
 */
class SoapService
{
    public const SOAP_ERROR_INVALID_CREDENTIALS = 1002;
    public const SOAP_ERROR_INVALID_INPUT = 1003;
    public const SOAP_ERROR_INVALID_TOKEN = 1004;
    public const SOAP_ERROR_UNEXPECTED = 1005;
    public const SOAP_ERROR_UNAUTHORIZED = 1006;

    private AuthService $authService;
    private CarRegistryAPIService $carRegistryAPIService;
    private StatisticsRepository $statisticsRepository;

    public function __construct(
        AuthService           $authService,
        CarRegistryAPIService $carRegistryAPIService,
        StatisticsRepository $statisticsRepository
    )
    {
        $this->authService = $authService;
        $this->carRegistryAPIService = $carRegistryAPIService;
        $this->statisticsRepository = $statisticsRepository;
    }

    protected function emitUnauthorizedError()
    {
        throw new SoapFault(self::SOAP_ERROR_UNAUTHORIZED, "You don't have right to perform this action!");
    }

    protected function stringifyValidationErrors(ValidatorInstance $validator): string
    {
        $errors = [];

        foreach ($validator->errors()->toArray() as $value)
            foreach ($value as $error)
                $errors[] = $error;


        return implode(" ", $errors);
    }

    /**
     * If user not found will throw soap fault
     */
    protected function getUserModelByToken(string $token): User
    {
        $user = $this->authService->getUserByToken($token);

        if ($user === null)
            throw new SoapFault(self::SOAP_ERROR_INVALID_TOKEN, "Invalid authentication token.");

        return $user;
    }

    protected function getDishModelById(int $id, array $with = [], array $withCount = []): Dish
    {
        $query = Dish::query();
        if (!empty($with)) $query->with($with);
        if (!empty($withCount)) $query->withCount($withCount);
        $dish = $query->where('id', $id)->get()->first();

        if ($dish === null)
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, "Given dish id does not exist!");

        return $dish;
    }

    protected function getFoodLogModelById(int $id, User $user, array $with = [], array $withCount = []): UserFoodLog
    {
        $query = UserFoodLog::query();
        if (!empty($with)) $query->with($with);
        if (!empty($withCount)) $query->withCount($withCount);
        if ($user->getAttribute('role') !== 'admin') $query->where('user_id', $user->getKey());
        $foodLog = $query->where('id', $id)->get()->first();

        if ($foodLog === null)
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, "Given food log id does not exist or is not visible to you!");

        return $foodLog;
    }

    protected function getCarModelById(int $id, User $user, array $with = [], array $withCount = []): UserCar
    {
        $query = UserCar::query();
        if (!empty($with)) $query->with($with);
        if (!empty($withCount)) $query->withCount($withCount);
        if ($user->getAttribute('role') !== 'admin') $query->where('user_id', $user->getKey());
        $car = $query->where('id', $id)->get()->first();

        if ($car === null)
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, "Given car id does not exist, or is not visible to you!");

        return $car;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $name")
     * @WSDL\Annotation\WebResult(param="string $greeting")
     */
    public function hello(string $name): string
    {
        return "Hello, " . $name;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $usernameRegister")
     * @WSDL\Annotation\WebParam(param="string $passwordRegister")
     * @WSDL\Annotation\WebResult(param="object $user { int $id string $username string $role string $created_at string $updated_at }")
     */
    public function register(string $usernameRegister, string $passwordRegister): UserResponse
    {
        $validator = Validator::make(
            [
                'username' => $usernameRegister,
                'password' => $passwordRegister
            ],
            AuthController::REGISTER_VALIDATE_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $user = $this->authService->register($usernameRegister, $passwordRegister);
        return new UserResponse($user);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $usernameLogin")
     * @WSDL\Annotation\WebParam(param="string $passwordLogin")
     * @WSDL\Annotation\WebResult(param="object $userTokenFromLogin { string $token string $created_from_ip string $valid_till }")
     */
    public function login(string $usernameLogin, string $passwordLogin): TokenResponse
    {
        try {
            $token = $this->authService->login($usernameLogin, $passwordLogin);
            return new TokenResponse($token);
        } catch (ModelNotFoundException | InvalidPasswordException | UserTokenCreationFailedException) {
            throw new SoapFault(self::SOAP_ERROR_INVALID_CREDENTIALS, 'Invalid credentials.');
        }
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $authTokenFetchUser")
     * @WSDL\Annotation\WebResult(param="object $userFetchedByToken { int $id string $username string $role string $created_at string $updated_at }")
     */
    public function getUserByToken(string $authTokenFetchUser): UserResponse
    {
        $user = $this->getUserModelByToken($authTokenFetchUser);

        return new UserResponse($user);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenToRefresh")
     * @WSDL\Annotation\WebResult(param="object $userTokenFromRefresh { string $token string $created_from_ip string $valid_till }")
     */
    public function refreshToken(string $tokenToRefresh): TokenResponse
    {
        $user = $this->getUserModelByToken($tokenToRefresh);

        try {
            $token = $this->authService->refreshTokenForUser($user, $tokenToRefresh);
        } catch (UserTokenCreationFailedException) {
            throw new SoapFault(self::SOAP_ERROR_UNEXPECTED, "Failed to create new token, please try again!");
        }

        return new TokenResponse($token);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenToDelete")
     * @WSDL\Annotation\WebParam(param="string $passwordToVerify")
     * @WSDL\Annotation\WebResult(param="boolean $userDeleteSuccess")
     */
    public function deleteUser(string $tokenToDelete, string $passwordToVerify): bool
    {
        $user = $this->getUserModelByToken($tokenToDelete);

        if (!Hash::check($passwordToVerify, $user->getAttribute('password')))
            throw new SoapFault(self::SOAP_ERROR_INVALID_CREDENTIALS, "Invalid password.");

        try {
            $this->authService->deleteUser($user);
        } catch (UserDeletionFailedException | QueryException) {
            throw new SoapFault(self::SOAP_ERROR_UNEXPECTED, "Failed to delete user, please try again!");
        }

        return true;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $dishPage")
     * @WSDL\Annotation\WebParam(param="int $dishPageSize")
     * @WSDL\Annotation\WebResult(param="object[] $dishListResponse { int $id string $title int $calories int $user_id string $created_at string $updated_at }")
     */
    public function listDishes(int $dishPage, int $dishPageSize): array
    {
        $page = max($dishPage, 0);
        $pageSize = max(min($dishPageSize, 1000), 0);

        $result = [];
        $dishes = Dish::query()->limit($pageSize)->offset($page * $pageSize)->get();
        foreach ($dishes->all() as $dish)
            $result[] = new DishResponse($dish);

        return $result;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $myDishPage")
     * @WSDL\Annotation\WebParam(param="int $myDishPageSize")
     * @WSDL\Annotation\WebParam(param="string $tokenMyDish")
     * @WSDL\Annotation\WebResult(param="object[] $myDishListResponse { int $id string $title int $calories int $user_id string $created_at string $updated_at }")
     */
    public function listMyDishes(int $myDishPage, int $myDishPageSize, string $tokenMyDish): array
    {
        $user = $this->getUserModelByToken($tokenMyDish);

        $page = max($myDishPage, 0);
        $pageSize = max(min($myDishPageSize, 1000), 0);

        $result = [];
        $dishes = Dish::query()->where('user_id', $user->getKey())
            ->limit($pageSize)->offset($page * $pageSize)->get();

        foreach ($dishes->all() as $dish)
            $result[] = new DishResponse($dish);

        return $result;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $dishIdToFetch")
     * @WSDL\Annotation\WebResult(param="object $dishGetResponse { int $id string $title int $calories int $user_id string $created_at string $updated_at }")
     */
    public function getDish(int $dishIdToFetch): DishResponse
    {
        $dish = $this->getDishModelById($dishIdToFetch);

        return new DishResponse($dish);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenCreateDish")
     * @WSDL\Annotation\WebParam(param="string $titleCreateDish")
     * @WSDL\Annotation\WebParam(param="int $caloriesCreateDish")
     * @WSDL\Annotation\WebResult(param="object $dishCreateResponse { int $id string $title int $calories int $user_id string $created_at string $updated_at }")
     */
    public function createDish(string $tokenCreateDish, string $titleCreateDish, int $caloriesCreateDish): DishResponse
    {
        $user = $this->getUserModelByToken($tokenCreateDish);

        $validator = Validator::make(
            [
                'title' => $titleCreateDish,
                'calories' => $caloriesCreateDish
            ],
            DishController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $data = $validator->validated();
        $data['user_id'] = $user->getKey();
        $dish = Dish::query()->create($data);

        return new DishResponse($dish);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenUpdateDish")
     * @WSDL\Annotation\WebParam(param="int $idUpdateDish")
     * @WSDL\Annotation\WebParam(param="string $titleUpdateDish")
     * @WSDL\Annotation\WebParam(param="int $caloriesUpdateDish")
     * @WSDL\Annotation\WebResult(param="object $dishUpdateResponse { int $id string $title int $calories int $user_id string $created_at string $updated_at }")
     */
    public function updateDish(string $tokenUpdateDish, int $idUpdateDish, string $titleUpdateDish, int $caloriesUpdateDish)
    {
        $user = $this->getUserModelByToken($tokenUpdateDish);

        $validator = Validator::make(
            [
                'title' => $titleUpdateDish,
                'calories' => $caloriesUpdateDish
            ],
            DishController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $dish = $this->getDishModelById($idUpdateDish);
        if (!DishController::hasUserPermissionOverModel($dish, $user))
            $this->emitUnauthorizedError();

        $dish->update($validator->validated());
        return new DishResponse($dish);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenDeleteDish")
     * @WSDL\Annotation\WebParam(param="int $idDeleteDish")
     * @WSDL\Annotation\WebResult(param="boolean $deleteDishSuccess")
     */
    public function deleteDish(string $tokenDeleteDish, int $idDeleteDish): bool
    {
        $user = $this->getUserModelByToken($tokenDeleteDish);

        $dish = $this->getDishModelById($idDeleteDish, [], ['logs']);
        if (!DishController::hasUserPermissionOverModel($dish, $user))
            $this->emitUnauthorizedError();

        if ($dish->getAttribute('logs_count') !== 0)
            throw new SoapFault(self::SOAP_ERROR_UNEXPECTED, "First, you must remove all logs referencing this dish!");

        $dish->deleteOrFail();
        return true;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $foodLogPage")
     * @WSDL\Annotation\WebParam(param="int $foodLogPageSize")
     * @WSDL\Annotation\WebParam(param="string $tokenListFoodLogs")
     * @WSDL\Annotation\WebResult(param="object[] $foodLogsListResponse { int $id string $created_at string $updated_at int $user_id int $dish_id string $eaten_at }")
     */
    public function listFoodLogs(int $foodLogPage, int $foodLogPageSize, string $tokenListFoodLogs): array
    {
        $user = $this->getUserModelByToken($tokenListFoodLogs);

        if ($user->getAttribute('role') !== 'admin')
            $this->emitUnauthorizedError();

        $page = max($foodLogPage, 0);
        $pageSize = max(min($foodLogPageSize, 1000), 0);

        $result = [];
        $models = UserFoodLog::query()->limit($pageSize)->offset($page * $pageSize)->get();

        foreach ($models as $model)
            $result[] = new FoodLogResponse($model);

        return $result;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $myFoodLogPage")
     * @WSDL\Annotation\WebParam(param="int $myFoodLogPageSize")
     * @WSDL\Annotation\WebParam(param="string $tokenListMyFoodLogs")
     * @WSDL\Annotation\WebResult(param="object[] $myFoodLogsListResponse { int $id string $created_at string $updated_at int $user_id int $dish_id string $eaten_at }")
     */
    public function listMyFoodLogs(int $myFoodLogPage, int $myFoodLogPageSize, string $tokenListMyFoodLogs): array
    {
        $user = $this->getUserModelByToken($tokenListMyFoodLogs);

        $page = max($myFoodLogPage, 0);
        $pageSize = max(min($myFoodLogPageSize, 1000), 0);

        $result = [];
        $models = UserFoodLog::query()->where('user_id', $user->getKey())
            ->limit($pageSize)->offset($page * $pageSize)->get();

        foreach ($models as $model)
            $result[] = new FoodLogResponse($model);

        return $result;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $foodLogIdToFetch")
     * @WSDL\Annotation\WebParam(param="string $tokenFetchFoodLog")
     * @WSDL\Annotation\WebResult(param="object $foodLogGetResponse { int $id string $created_at string $updated_at int $user_id int $dish_id string $eaten_at }")
     */
    public function getFoodLog(int $foodLogIdToFetch, string $tokenFetchFoodLog): FoodLogResponse
    {
        $user = $this->getUserModelByToken($tokenFetchFoodLog);
        $foodLog = $this->getFoodLogModelById($foodLogIdToFetch, $user);

        return new FoodLogResponse($foodLog);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenCreateFoodLog")
     * @WSDL\Annotation\WebParam(param="int $dishIdCreateFoodLog")
     * @WSDL\Annotation\WebParam(param="string $eatenAtCreateFoodLog")
     * @WSDL\Annotation\WebResult(param="object $foodLogCreateResponse { int $id string $created_at string $updated_at int $user_id int $dish_id string $eaten_at }")
     */
    public function createFoodLog(string $tokenCreateFoodLog, int $dishIdCreateFoodLog, string $eatenAtCreateFoodLog): FoodLogResponse
    {
        $user = $this->getUserModelByToken($tokenCreateFoodLog);

        $validator = Validator::make(
            [
                'dish_id' => $dishIdCreateFoodLog,
                'eaten_at' => $eatenAtCreateFoodLog,
            ],
            FoodLogController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $data = $validator->validated();
        $data['user_id'] = $user->getKey();

        /** @var UserFoodLog $foodLog */
        $foodLog = UserFoodLog::query()->create($data);

        return new FoodLogResponse($foodLog);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $foodLogToUpdateId")
     * @WSDL\Annotation\WebParam(param="string $tokenUpdateFoodLog")
     * @WSDL\Annotation\WebParam(param="int $dishIdUpdateFoodLog")
     * @WSDL\Annotation\WebParam(param="string $eatenAtUpdateFoodLog")
     * @WSDL\Annotation\WebResult(param="object $foodLogUpdateResponse { int $id string $created_at string $updated_at int $user_id int $dish_id string $eaten_at }")
     */
    public function updateFoodLog(int $foodLogToUpdateId, string $tokenUpdateFoodLog, int $dishIdUpdateFoodLog, string $eatenAtUpdateFoodLog): FoodLogResponse
    {
        $user = $this->getUserModelByToken($tokenUpdateFoodLog);
        $foodLog = $this->getFoodLogModelById($foodLogToUpdateId, $user);

        $validator = Validator::make(
            [
                'dish_id' => $dishIdUpdateFoodLog,
                'eaten_at' => $eatenAtUpdateFoodLog,
            ],
            FoodLogController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $data = $validator->validated();
        $foodLog->update($data);

        return new FoodLogResponse($foodLog);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenDeleteFoodLog")
     * @WSDL\Annotation\WebParam(param="int $idDeleteFoodLog")
     * @WSDL\Annotation\WebResult(param="boolean $deleteFoodLogSuccess")
     */
    public function deleteFoodLog(string $tokenDeleteFoodLog, int $idDeleteFoodLog): bool
    {
        $user = $this->getUserModelByToken($tokenDeleteFoodLog);
        $foodLog = $this->getFoodLogModelById($idDeleteFoodLog, $user);
        $foodLog->deleteOrFail();
        return true;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $carsPage")
     * @WSDL\Annotation\WebParam(param="int $carsPageSize")
     * @WSDL\Annotation\WebParam(param="string $tokenCarsList")
     * @WSDL\Annotation\WebResult(param="object[] $carsListResponse { int $id string $created_at string $updated_at int $user_id string $license_plate int $car_registry_id }")
     */
    public function listCars(int $carsPage, int $carsPageSize, string $tokenCarsList): array
    {
        $user = $this->getUserModelByToken($tokenCarsList);

        if ($user->getAttribute('role') !== 'admin')
            $this->emitUnauthorizedError();

        $page = max($carsPage, 0);
        $pageSize = max(min($carsPageSize, 1000), 0);

        $result = [];
        $models = UserCar::query()->limit($pageSize)->offset($page * $pageSize)->get();

        foreach ($models as $model)
            $result[] = new UserCarResponse($model);

        return $result;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $myCarsPage")
     * @WSDL\Annotation\WebParam(param="int $myCarsPageSize")
     * @WSDL\Annotation\WebParam(param="string $myTokenCarsList")
     * @WSDL\Annotation\WebResult(param="object[] $carsMyListResponse { int $id string $created_at string $updated_at int $user_id string $license_plate int $car_registry_id }")
     */
    public function listMyCars(int $myCarsPage, int $myCarsPageSize, string $myTokenCarsList): array
    {
        $user = $this->getUserModelByToken($myTokenCarsList);

        $page = max($myCarsPage, 0);
        $pageSize = max(min($myCarsPageSize, 1000), 0);

        $result = [];
        $models = UserCar::query()->where('user_id', $user->getKey())
            ->limit($pageSize)->offset($page * $pageSize)->get();

        foreach ($models as $model)
            $result[] = new UserCarResponse($model);

        return $result;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $carIdToFetch")
     * @WSDL\Annotation\WebParam(param="string $tokenGetCar")
     * @WSDL\Annotation\WebResult(param="object $carGetResponse { int $id string $created_at string $updated_at int $user_id string $license_plate int $car_registry_id object $user { int $id string $username string $role string $created_at string $updated_at } object $registry { int $id string $brand string $model string $country int $year } }")
     */
    public function getCar(int $carIdToFetch, string $tokenGetCar): UserCarComposedResponse
    {
        $user = $this->getUserModelByToken($tokenGetCar);
        $car = $this->getCarModelById($carIdToFetch, $user, ['user']);
        $carRegistry = $this->carRegistryAPIService->getCar($car->getAttribute('car_registry_id'));

        return new UserCarComposedResponse($car, $carRegistry);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $carRegistryIdCreateCar")
     * @WSDL\Annotation\WebParam(param="string $licensePlateCreateCar")
     * @WSDL\Annotation\WebParam(param="string $tokenCreateCar")
     * @WSDL\Annotation\WebResult(param="object $carCreateResponse { int $id string $created_at string $updated_at int $user_id string $license_plate int $car_registry_id }")
     */
    public function createCar(int $carRegistryIdCreateCar, string $licensePlateCreateCar, string $tokenCreateCar): UserCarResponse
    {
        $user = $this->getUserModelByToken($tokenCreateCar);

        $validator = Validator::make(
            [
                'license_plate' => $licensePlateCreateCar,
                'car_registry_id' => $carRegistryIdCreateCar
            ],
            UserCarController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $data = $validator->validated();
        $data['user_id'] = $user->getKey();

        /** @var UserCar $car */
        $car = UserCar::query()->create($data);
        return new UserCarResponse($car);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $licensePlateCreateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $brandCreateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $modelCreateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $countryCreateCarComposed")
     * @WSDL\Annotation\WebParam(param="int $yearCreateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $tokenCreateCarComposed")
     * @WSDL\Annotation\WebResult(param="object $carCreateComposedResponse { int $id string $created_at string $updated_at int $user_id string $license_plate int $car_registry_id }")
     */
    public function createCarComposed(
        string $licensePlateCreateCarComposed,
        string $brandCreateCarComposed,
        string $modelCreateCarComposed,
        string $countryCreateCarComposed,
        int    $yearCreateCarComposed,
        string $tokenCreateCarComposed
    ): UserCarResponse
    {
        //We fetch user here just to validate the token.
        $user = $this->getUserModelByToken($tokenCreateCarComposed);

        $validator = Validator::make(
            [
                'license_plate' => $licensePlateCreateCarComposed,
                'car_registry_brand' => $brandCreateCarComposed,
                'car_registry_model' => $modelCreateCarComposed,
                'car_registry_country' => $countryCreateCarComposed,
                'car_registry_year' => $yearCreateCarComposed,
            ],
            UserCarController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $data = $validator->validated();
        $carRegistryId = $this->carRegistryAPIService->createCar(
            $data['car_registry_brand'],
            $data['car_registry_model'],
            $data['car_registry_country'],
            $data['car_registry_year'],
        );

        if ($carRegistryId === null)
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, "Failed to create registry entry in child service!");

        return $this->createCar($carRegistryId, $licensePlateCreateCarComposed, $tokenCreateCarComposed);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $carToUpdateId")
     * @WSDL\Annotation\WebParam(param="int $carRegistryIdUpdateCar")
     * @WSDL\Annotation\WebParam(param="string $licensePlateUpdateCar")
     * @WSDL\Annotation\WebParam(param="string $tokenUpdateCar")
     * @WSDL\Annotation\WebResult(param="object $carUpdateResponse { int $id string $created_at string $updated_at int $user_id string $license_plate int $car_registry_id }")
     */
    public function updateCar(int $carToUpdateId, int $carRegistryIdUpdateCar, string $licensePlateUpdateCar, string $tokenUpdateCar): UserCarResponse
    {
        $user = $this->getUserModelByToken($tokenUpdateCar);
        $car = $this->getCarModelById($carToUpdateId, $user);

        $validator = Validator::make(
            [
                'license_plate' => $licensePlateUpdateCar,
                'car_registry_id' => $carRegistryIdUpdateCar
            ],
            UserCarController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $data = $validator->validated();
        $car->update($data);

        return new UserCarResponse($car);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $carToUpdateComposedId")
     * @WSDL\Annotation\WebParam(param="string $licensePlateUpdateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $brandUpdateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $modelUpdateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $countryUpdateCarComposed")
     * @WSDL\Annotation\WebParam(param="int $yearUpdateCarComposed")
     * @WSDL\Annotation\WebParam(param="string $tokenUpdateCarComposed")
     * @WSDL\Annotation\WebResult(param="object $carUpdateComposedResponse { int $id string $created_at string $updated_at int $user_id string $license_plate int $car_registry_id }")
     */
    public function updateCarComposed(
        int    $carToUpdateComposedId,
        string $licensePlateUpdateCarComposed,
        string $brandUpdateCarComposed,
        string $modelUpdateCarComposed,
        string $countryUpdateCarComposed,
        int    $yearUpdateCarComposed,
        string $tokenUpdateCarComposed
    )
    {
        //We fetch user here just to validate the token.
        $user = $this->getUserModelByToken($tokenUpdateCarComposed);
        $car = $this->getCarModelById($carToUpdateComposedId, $user);

        $validator = Validator::make(
            [
                'license_plate' => $licensePlateUpdateCarComposed,
                'car_registry_brand' => $brandUpdateCarComposed,
                'car_registry_model' => $modelUpdateCarComposed,
                'car_registry_country' => $countryUpdateCarComposed,
                'car_registry_year' => $yearUpdateCarComposed,
            ],
            UserCarController::MODEL_RULES
        );

        if ($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        $data = $validator->validated();
        $result = $this->carRegistryAPIService->updateCar(
            $car->getAttribute('car_registry_id'),
            $data['car_registry_brand'],
            $data['car_registry_model'],
            $data['car_registry_country'],
            $data['car_registry_year'],
        );

        if (!$result)
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, "Either CarRegistry entry did not change, or we couldn't connect to child service!");

        return $this->updateCar(
            $carToUpdateComposedId,
            $car->getAttribute('car_registry_id'),
            $licensePlateUpdateCarComposed,
            $tokenUpdateCarComposed
        );
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="int $carIdToDelete")
     * @WSDL\Annotation\WebParam(param="string $tokenCarDelete")
     * @WSDL\Annotation\WebResult(param="boolean $deleteCarSuccess")
     */
    public function deleteCar(int $carIdToDelete, string $tokenCarDelete): bool
    {
        $user = $this->getUserModelByToken($tokenCarDelete);
        $car = $this->getCarModelById($carIdToDelete, $user);

        $car->deleteOrFail();
        return true;
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenToCalculateCalories")
     * @WSDL\Annotation\WebParam(param="string $timeFromCalories")
     * @WSDL\Annotation\WebParam(param="string $timeUntilCalories")
     * @WSDL\Annotation\WebResult(param="float $myTotalCalories")
     */
    public function calculateMyTotalCalories(string $tokenToCalculateCalories, string $timeFromCalories, string $timeUntilCalories): float
    {
        $user = $this->getUserModelByToken($tokenToCalculateCalories);

        $validator = Validator::make(
            [
                'from' => $timeFromCalories,
                'until' => $timeUntilCalories
            ],
            StatisticsController::TIMESPAN_RULES
        );

        if($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        return $this->statisticsRepository->calculateUserCalories($user, $timeFromCalories, $timeUntilCalories);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenToCalculateCars")
     * @WSDL\Annotation\WebResult(param="int $myTotalCars")
     */
    public function calculateMyTotalCars(string $tokenToCalculateCars): int
    {
        $user = $this->getUserModelByToken($tokenToCalculateCars);
        return $this->statisticsRepository->calculateUserCars($user);
    }

    /**
     * @WSDL\Annotation\WebMethod()
     * @WSDL\Annotation\WebParam(param="string $tokenToCalculateDishes")
     * @WSDL\Annotation\WebParam(param="string $timeFromDishes")
     * @WSDL\Annotation\WebParam(param="string $timeUntilDishes")
     * @WSDL\Annotation\WebResult(param="object[] $myTotalDishes { string $title int $count }")
     */
    public function calculateMyTotalDishes(string $tokenToCalculateDishes, string $timeFromDishes, string $timeUntilDishes): array
    {
        $user = $this->getUserModelByToken($tokenToCalculateDishes);

        $validator = Validator::make(
            [
                'from' => $timeFromDishes,
                'until' => $timeUntilDishes
            ],
            StatisticsController::TIMESPAN_RULES
        );

        if($validator->fails())
            throw new SoapFault(self::SOAP_ERROR_INVALID_INPUT, $this->stringifyValidationErrors($validator));

        return $this->statisticsRepository->calculateUserDishes($user, $timeFromDishes, $timeUntilDishes)->toArray();
    }
}
