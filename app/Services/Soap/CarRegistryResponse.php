<?php

namespace App\Services\Soap;

class CarRegistryResponse
{
    public int $id;
    public string $brand;
    public string $model;
    public string $country;
    public int $year;

    public function __construct(array $registry)
    {
        $this->id = (int) $registry['id'];
        $this->brand = $registry['brand'];
        $this->model = $registry['model'];
        $this->country = $registry['country'];
        $this->year = (int) $registry['year'];
    }
}
