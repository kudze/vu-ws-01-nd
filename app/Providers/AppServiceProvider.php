<?php

namespace App\Providers;

use App\Validators\CarRegistryValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Validator::extend('exists_car_registry_id', CarRegistryValidator::class . '@validateCarRegistryId');
    }
}
