<?php

namespace App\Console;

use App\Console\Commands\CompileWsdlCommand;
use App\Console\Commands\SetUserRoleCommand;
use App\Console\Commands\UserCreateCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SetUserRoleCommand::class,
        UserCreateCommand::class,
        CompileWsdlCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
