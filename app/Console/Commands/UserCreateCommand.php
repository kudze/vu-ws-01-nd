<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserCreateCommand extends Command
{
    protected $signature = "user:create {username} {password}";
    protected $description = "Creates an user!";

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');

        try {
            $output->writeln("User with username $username was created!");

            User::query()->create(
                [
                    'username' => $username,
                    'password' => Hash::make($password)
                ]
            );

        } catch(QueryException $e) {
            $output->writeln("Failed to create user with username $username!");
            $output->writeln($e->getMessage());
        }

        return self::SUCCESS;
    }
}
