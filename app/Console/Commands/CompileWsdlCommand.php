<?php

namespace App\Console\Commands;

use App\Services\Soap\SoapService;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use WSDL\Builder\AnnotationWSDLBuilder;
use WSDL\WSDL;

class CompileWsdlCommand extends Command
{
    protected $signature = "wsdl:compile";
    protected $description = "Compiles the file food-log.wsdl from SoapService annotations!";

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title("Running wsdl:compile...");

        $io->info("Compiling...");
        $wsdl = new AnnotationWSDLBuilder(SoapService::class);
        $wsdl->build();
        $xml = WSDL::fromBuilder($wsdl->getBuilder())->create();

        $io->info("Writing to food-log.wsdl...");
        file_put_contents(__DIR__ . '/../../../food-log.wsdl', $xml);

        $io->success("food-log.wsdl has been compiled!");
        return self::SUCCESS;
    }
}
