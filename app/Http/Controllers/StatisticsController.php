<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\StatisticsRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class StatisticsController extends Controller
{
    public const TIMESPAN_RULES = [
        'from' => 'required|date_format:Y-m-d H:i:s',
        'until' => 'required|date_format:Y-m-d H:i:s|after:from'
    ];

    private StatisticsRepository $statisticsRepository;

    public function __construct(StatisticsRepository $statisticsRepository)
    {
        $this->middleware("auth");

        $this->statisticsRepository = $statisticsRepository;
    }

    public function totalMyCalories(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        $data = $this->validateStatisticsData($request);
        $calories = $this->statisticsRepository->calculateUserCalories($user, $data['from'], $data['until']);

        return response()->json($calories);
    }

    public function countMyDishes(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        $data = $this->validateStatisticsData($request);
        $counts = $this->statisticsRepository->calculateUserDishes($user, $data['from'], $data['until']);

        return response()->json($counts->toArray());
    }

    public function totalCars(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        $cars = $this->statisticsRepository->calculateUserCars($user);

        return response()->json($cars);
    }

    /**
     * @throws ValidationException
     */
    protected function validateStatisticsData(Request $request): array
    {
        $data = $request->query->all();

        $validator = Validator::make(
            $data,
            self::TIMESPAN_RULES
        );

        return $validator->validate();
    }
}
