<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function fetchPaginationInput(Request $request): array
    {
        $page = max($request->input('page', 0), 0);
        $pageSize = max(min($request->input('pageSize', 50), 1000), 0);

        return ['page' => $page, 'size' => $pageSize];
    }

    protected function unauthorizedResponse() : Response|ResponseFactory
    {
        return response('Unauthorized.', 401);
    }

    protected function okResponse() : Response|ResponseFactory
    {
        return response('', 204);
    }
}
