<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidPasswordException;
use App\Exceptions\UserDeletionFailedException;
use App\Exceptions\UserTokenCreationFailedException;
use App\Models\Dish;
use App\Models\User;
use App\Models\UserCar;
use App\Models\UserFoodLog;
use App\Models\UserToken;
use App\Services\AuthService;
use App\Services\UserTokenFactory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class AuthController extends Controller
{
    public const REGISTER_VALIDATE_RULES = [
        'username' => "required|min:3|max:64|unique:user,username",
        'password' => "required|min:3|max:1024"
    ];

    private AuthService $authService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
        $this->middleware('auth', [
            'only' => [
                'refresh',
                'me',
                'delete'
            ]
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $data = $this->validate($request, self::REGISTER_VALIDATE_RULES);
        $user = $this->authService->register($data['username'], $data['password']);
        return response()->json($user->toArray(), 201);
    }

    public function login(Request $request): JsonResponse
    {
        $data = $this->validate(
            $request,
            [
                'username' => "required|min:3|max:64",
                'password' => "required|min:3|max:1024"
            ]
        );

        try {
            $token = $this->authService->login($data['username'], $data['password']);
            return response()->json($token->toArray(), 201);
        } catch (ModelNotFoundException | InvalidPasswordException | UserTokenCreationFailedException) {
            throw ValidationException::withMessages(["username" => "Invalid credentials!"]);
        }
    }

    public function refresh(Request $request, UserTokenFactory $tokenFactory): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        try {
            $currentTokenValue = str_replace("Bearer ", "", $request->header('Authorization'));
            $token = $this->authService->refreshTokenForUser($user, $currentTokenValue);
            return response()->json($token->toArray(), 201);
        } catch (UserTokenCreationFailedException) {
            throw ValidationException::withMessages(["api_token" => "Please try again later!"]);
        }
    }

    public function me(): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        return response()->json($user->toArray());
    }

    public function delete(Request $request): Response|ResponseFactory
    {
        ['password' => $password] = $this->validate(
            $request,
            [
                'password' => "required|min:3|max:1024"
            ]
        );

        try {
            /** @var User $user */
            $user = Auth::user();

            if (!Hash::check($password, $user->getAttribute('password')))
                throw new InvalidPasswordException();

            $this->authService->deleteUser($user);
        } catch (ModelNotFoundException | InvalidPasswordException | UserTokenCreationFailedException) {
            throw ValidationException::withMessages(["username" => "Invalid credentials!"]);
        } catch (UserDeletionFailedException | QueryException) {
            throw ValidationException::withMessages(["username" => "Try again later..."]);
        }

        return $this->okResponse();
    }
}
