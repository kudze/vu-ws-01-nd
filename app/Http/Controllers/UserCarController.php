<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserCar;
use App\Services\CarRegistryAPIService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class UserCarController extends Controller
{
    public const MODEL_RULES = [
        'car_registry_id' => 'required_without:car_registry_brand,car_registry_model,car_registry_country,car_registry_year|integer|exists_car_registry_id',
        'car_registry_brand' => 'required_without:car_registry_id|string|max:50',
        'car_registry_model' => 'required_without:car_registry_id|string|max:50',
        'car_registry_country' => 'required_without:car_registry_id|string|max:50',
        'car_registry_year' => 'required_without:car_registry_id|integer',
        'license_plate' => 'required|string|size:6|unique:user_car',
    ];

    private CarRegistryAPIService $carRegistryAPIService;

    public function __construct(CarRegistryAPIService $carRegistryAPIService)
    {
        $this->middleware('auth');
        $this->middleware('role:admin', ['only' => ['index']]);

        $this->carRegistryAPIService = $carRegistryAPIService;
    }

    public function index(Request $request): JsonResponse
    {
        [
            'page' => $page,
            'size' => $pageSize
        ] = $this->fetchPaginationInput($request);

        $models = UserCar::query()->limit($pageSize)->offset($page * $pageSize)->get();
        return response()->json($models->toArray());
    }

    public function myIndex(Request $request): JsonResponse
    {
        [
            'page' => $page,
            'size' => $pageSize
        ] = $this->fetchPaginationInput($request);

        /** @var User $user */
        $user = Auth::user();

        $models = UserCar::query()->where('user_id', $user->getKey())
            ->limit($pageSize)->offset($page * $pageSize)->get();

        return response()->json($models->toArray());
    }

    public function get(int $id): JsonResponse|ResponseFactory
    {
        /** @var UserCar $model */
        $model = UserCar::query()->with('user')->where('id', $id)->first();
        if ($model === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        if (!$this->hasClientPermissionOverModel($model))
            return $this->unauthorizedResponse();

        $array = $model->toArray();
        $carInfo = $this->carRegistryAPIService->getCar($model->getAttribute('car_registry_id'));
        $array['extensions'] = ['registry' => $carInfo, 'user' => $model->user];
        unset($array['user']);
        return response()->json($array);
    }

    public function create(Request $request): JsonResponse|ResponseFactory|Response
    {
        $data = $this->validateModelData($request);

        /** @var User $user */
        $user = Auth::user();
        $data['user_id'] = $user->getKey();

        if (!array_key_exists('car_registry_id', $data)) {
            $data['car_registry_id'] = $this->carRegistryAPIService->createCar(
                $data['car_registry_brand'],
                $data['car_registry_model'],
                $data['car_registry_country'],
                $data['car_registry_year'],
            );

            if($data['car_registry_id'] === null)
                return response("Nepavyko sukurti registro informacijos!", 500);
        }

        $model = UserCar::query()->create($data);
        return response()->json($model->toArray());
    }

    public function update(int $id, Request $request): Response|ResponseFactory|JsonResponse
    {
        $data = $this->validateModelData($request, [
            'license_plate' => [
                'required',
                'string',
                'size:6',
                Rule::unique('user_car')->ignore($id)
            ],
        ]);

        /** @var UserCar $model */
        $model = UserCar::query()->find($id);
        if ($model === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        if (!$this->hasClientPermissionOverModel($model))
            return $this->unauthorizedResponse();

        if (!array_key_exists('car_registry_id', $data)) {
            $result = $this->carRegistryAPIService->updateCar(
                $model->getAttribute('car_registry_id'),
                $data['car_registry_brand'],
                $data['car_registry_model'],
                $data['car_registry_country'],
                $data['car_registry_year'],
            );

            if(!$result)
                return response("Nepavyko atnaujinti registro informacijos!", 500);
        }

        $model->update($data);

        return response()->json($model->toArray());
    }

    public function delete(int $id): Response|ResponseFactory
    {
        /** @var UserCar $model */
        $model = UserCar::query()->find($id);
        if ($model === null)
            throw ValidationException::withMessages(['id' => "invalid id"]);

        if (!$this->hasClientPermissionOverModel($model))
            return $this->unauthorizedResponse();

        $model->deleteOrFail();

        return $this->okResponse();
    }

    protected function hasClientPermissionOverModel(UserCar $model): bool
    {
        return $model->getAttribute('user_id') === Auth::id() || Auth::user()->getAttribute('role') === 'admin';
    }

    protected function validateModelData(Request $request, array $rules = [])
    {
        return $this->validate($request,
            array_merge(
                self::MODEL_RULES,
                $rules
            )
        );
    }
}
