<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $role = 'default')
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user?->getAttribute('role') !== $role)
            return response('Unauthorized.', 401);

        return $next($request);
    }
}
