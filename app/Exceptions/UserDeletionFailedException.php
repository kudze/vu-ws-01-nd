<?php

namespace App\Exceptions;

use RuntimeException;

class UserDeletionFailedException extends RuntimeException
{

}
