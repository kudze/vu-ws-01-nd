<?php

namespace App\Validators;

use App\Services\CarRegistryAPIService;

class CarRegistryValidator
{
    private CarRegistryAPIService $carRegistryAPIService;

    public function __construct(CarRegistryAPIService $carRegistryAPIService)
    {
        $this->carRegistryAPIService = $carRegistryAPIService;
    }

    public function validateCarRegistryId($attribute, $value, $parameters): bool
    {
        if (!is_numeric($value))
            return false;

        $value = (int)$value;
        return $this->carRegistryAPIService->getCar($value) !== null;
    }

}
